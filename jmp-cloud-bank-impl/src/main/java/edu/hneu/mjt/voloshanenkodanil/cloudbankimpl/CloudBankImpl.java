package edu.hneu.mjt.voloshanenkodanil.cloudbankimpl;


import edu.hneu.mjt.voloshanenkodanil.bankapi.Bank;
import edu.hneu.mjt.voloshanenkodanil.dto.*;

import java.util.Random;

public class CloudBankImpl implements Bank {
    private final Random random = new Random();
    @Override
    public BankCard createBankCard(User user, BankCardType bankCardType) {
        return switch (bankCardType){
            case CREDIT -> new CreditBankCard(user, generateCardNumber(), random.nextDouble(150000));
            case DEBIT -> new DebitBankCard(user, generateCardNumber(), random.nextDouble(150000),
                    random.nextDouble(15000));
        };
    }

    public String generateCardNumber() {
        StringBuilder cardNumber = new StringBuilder();
        cardNumber.append("4");
        for (int i = 1; i < 16; i++) {
            cardNumber.append(random.nextInt(10));
        }
        return cardNumber.toString();
    }
}


