package edu.hneu.mjt.voloshanenkodanil.dto;

public enum BankCardType {
    CREDIT,
    DEBIT
}

