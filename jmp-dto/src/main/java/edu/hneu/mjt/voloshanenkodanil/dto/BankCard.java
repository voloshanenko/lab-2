package edu.hneu.mjt.voloshanenkodanil.dto;

public class BankCard {
    private String number;
    private User user;
    private double balance;
    private double creditLimit;
    public BankCard(User user, String cardNumber, double balance, double creditLimit) {
        this.user = user;
        this.number = cardNumber;
        this.balance = balance;
        this.creditLimit = creditLimit;
    }
    public BankCard(User user, String cardNumber, double balance) {
        this.user = user;
        this.number = cardNumber;
        this.balance = balance;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public double getBalance() {
        return balance;
    }
    public String getNumber() {
        return number;
    }

    public User getUser() {
        return user;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void setNumber(String number) {
        this.number = number;
    }

    public void setUser(User user) {
        this.user = user;
    }

}

