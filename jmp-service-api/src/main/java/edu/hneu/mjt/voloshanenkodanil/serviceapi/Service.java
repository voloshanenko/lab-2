package edu.hneu.mjt.voloshanenkodanil.serviceapi;

import edu.hneu.mjt.voloshanenkodanil.dto.BankCard;
import edu.hneu.mjt.voloshanenkodanil.dto.Subscription;
import edu.hneu.mjt.voloshanenkodanil.dto.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface Service {
    void subscribe(BankCard bankCard, LocalDate startDate);

    Optional<Subscription> getSubscriptionByBankCardNumber(String bankCardNumber);

    List<User> getAllUsers();
}


