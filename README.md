<h2>Лабораторна робота 2</h2>

<h3>Тема: "ДОСЛІДЖЕННЯ ДОДАТКОВОГО ФУНКЦІОНАЛУ В JAVA 8-17"</h3>

</h4>Виконав: Волошаненко Даніл</h4>
 
**Створити maven проекту з 5-ю модулями:**

1. jmp-bank-api
1. jmp-service-api
1. jmp-cloud-bank-impl
1. jmp-dto1. 
1. jmp-cloud-service-impl

#
![Скриншот 1](img/photo_2024-04-24_09-34-34.jpg)
**створив проект**
#
![Скриншот 2](img/1.png)
**створив модулі**

**Створіть наступні класи в модулі jmp-dto:**
1. [User] 
String name;String surmane;
LocalDate birthday;
1. [BankCard]
String number;
User user;
1. [Subscription]
String bankcard;
LocaleDate startDate;
#
![Скриншот 1](img/photo_2024-04-24_09-37-06.jpg)
**Створені класи в dto**

**Подовжте клас BankCard за допомогою:**
1. CreditBankCard
1. DebitBankCard

#
![Скриншот 1](img/photo_2024-04-24_09-37-46.jpg)
Подовжив клас BankCard
- 
**Створити enum :**
- [BankCardType]
- CREDIT
- DEBIT

![Скриншот 1](img/photo_2024-04-24_09-38-36.jpg)
Створив Enum
-
**Додайте інтерфейс Bank до jmp-bank-api за допомогою:**
BankCard createBankCard(User, BankCardType)

![Скриншот 1](img/photo_2024-04-24_09-39-01.jpg)
Створив інтерфейс в заданому модулі
-
Додайте module-info.java за допомогою
-
- requires jmp-dto
- export Bank interface


![Скриншот 1](img/photo_2024-04-24_09-39-24.jpg)

Створив module-info в модулі jmp-bank-api
-

**Впровадити Bank в jmp-cloud-bank-impl.
Метод повинен створювати новий клас в залежності від типу**

![Скриншот 1](img/photo_2024-04-24_09-41-16.jpg)

Створення відповідної карти за типом
-

Додайте module-info.java який містить:
-
- requires transitive module with Bank interface
- requires jmp-dto
- export implementation of Bank interface
![Скриншот 1](img/photo_2024-04-24_09-41-50.jpg)

Створив module-info в модулі jmp-cloud-bank-impl
-

Додайте інтерфейс Service до jmp-service-api за допомогою:
-
- void subscribe(BankCard);
- Optional getSubscriptionByBankCardNumber(String);
- List getAllUsers();

![Скриншот 1](img/photo_2024-04-24_09-42-58.jpg)

Створив інтерфейс в відповідному модулі


Створив module-info в модулі jmp-service-api
-
 Впровадити Service в jmp-cloud-service-impl. API потоку користувача. Ви можете використовувати Map або Mongo/БД для перегляду даних.
 -

![Скриншот 1](img/photo_2024-04-24_09-43-32.jpg)

Впровадив Service в CloudServiceImpl
-
Додайте module-info.java з налаштуваннями:
-
- requires transitive module with Service interface
- requires jmp-dto
- export implementation of Service interfaces
-
Використовуйте var для визначення локальних змінних, де б це не було застосовано.
-
Створити модуль основного додатку.
-
Використовуйте лямбди та функції Java 8, де б це не було застосовано.
-

![Скриншот 1](img/photo_2024-04-24_09-53-55.jpg)

Приклад роботи в консолі
-
![Скриншот 1](img/photo_2024-04-24_09-53-05.jpg)



