package edu.hneu.mjt.voloshanenkodanil.bankapi;

import edu.hneu.mjt.voloshanenkodanil.dto.BankCard;
import edu.hneu.mjt.voloshanenkodanil.dto.BankCardType;
import edu.hneu.mjt.voloshanenkodanil.dto.User;

public interface Bank {
    BankCard createBankCard(User user, BankCardType bankCardType);
}


