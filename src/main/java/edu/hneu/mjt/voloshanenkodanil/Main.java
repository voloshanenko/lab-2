package edu.hneu.mjt.voloshanenkodanil;

import edu.hneu.mjt.voloshanenkodanil.bankapi.Bank;
import edu.hneu.mjt.voloshanenkodanil.cloudbankimpl.CloudBankImpl;
import edu.hneu.mjt.voloshanenkodanil.cloudserviceimpl.CloudServiceImpl;
import edu.hneu.mjt.voloshanenkodanil.dto.BankCardType;
import edu.hneu.mjt.voloshanenkodanil.dto.Subscription;
import edu.hneu.mjt.voloshanenkodanil.dto.User;
import edu.hneu.mjt.voloshanenkodanil.serviceapi.Service;

import java.time.LocalDate;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Bank bank = new CloudBankImpl();
        Service service = new CloudServiceImpl();

        var user1 = new User("Іван", "Петров", LocalDate.of(1990, 5, 15));
        var user2 = new User("Марія", "Іванова", LocalDate.of(1985, 9, 20));
        var user3 = new User("Костянтин", "Шевченко", LocalDate.of(2002, 8, 2));
        var card1 = bank.createBankCard(user1, BankCardType.CREDIT);
        var card2 = bank.createBankCard(user2, BankCardType.DEBIT);
        var card3 = bank.createBankCard(user3, BankCardType.DEBIT);
        service.subscribe(card1, LocalDate.now());
        service.subscribe(card2, LocalDate.now());
        service.subscribe(card3, LocalDate.now());
        var users = service.getAllUsers();
        users.forEach(user -> {
            System.out.println(user.getSurname() + " " + user.getName() + " " + user.getBirthday());
        });
        Optional<Subscription> subscription1 = service.getSubscriptionByBankCardNumber(card1.getNumber());
        Optional<Subscription> subscription2 = service.getSubscriptionByBankCardNumber(card2.getNumber());
        subscription1.ifPresent(sub -> System.out.println("\nПідписка 1:\nномер карти: " + sub.getBankcard() +
                ",\nчас реєстрації: " + sub.getStartDate()));
        subscription2.ifPresent(sub -> System.out.println("\nПідписка 2:\nномер карти: " + sub.getBankcard() +
                ",\nчас реєстрації: " + sub.getStartDate()));
    }
}

